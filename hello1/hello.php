<form>
    What is your name? <input type="text" name="name"><br>
    What is your age? <input type="number" name="age"><br>
    <input type="submit" value="Say Hello">
</form>
<?php

if (isset($_GET['name'])){
    $name = $_GET['name'];
    $age = $_GET['age'];
    file_put_contents("people.txt", "$name;$age\n", FILE_APPEND | LOCK_EX);
   echo "Hello $name, you are ,$age y/o!";
}

// display previous names and ages
echo "<p>Previous people we said hello to:</p>\n";
echo "<ul>\n";
// read lines of file into an array of strings/lines
$fileLines = file("people.txt");
// show from newest to oldest (reverse order)
array_reverse($fileLines);
foreach ($fileLines as $line) {
    $data = split(";", $line);
    $name = $data[0];
    $age = $data[1];
    echo "<li>$name is $age y/o</li>\n";
}
echo "<ul>\n";