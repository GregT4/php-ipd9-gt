<?php

/* admin_todos_list.html.twig */
class __TwigTemplate_38b1520f50b0b2242b99e2bd0e07d79e6b61a7f615865944873453bb8c783f03 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "admin_todos_list.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Todo List";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "
<a href=\"/admin/list/add\">Add Todo</a><br><br>
      <table border=\"1\">
          <tr><th>Name</th><th>Task</th><th>Due Date</th><th>Is Done</th></tr>
          ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["todoList"]) ? $context["todoList"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["t"]) {
            // line 11
            echo "              <tr><td>";
            echo twig_escape_filter($this->env, $this->getAttribute($context["t"], "name", array()), "html", null, true);
            echo "</td>
                  <td>";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($context["t"], "task", array()), "html", null, true);
            echo "</td>
                  <td>";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($context["t"], "dueDate", array()), "html", null, true);
            echo "</td>
                  <td>";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["t"], "isDone", array()), "html", null, true);
            echo "</td>
                  <td><a href=\"/>admin/todo/edit/";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["t"], "id", array()), "html", null, true);
            echo "\">Edit<a/>
                      <a href=\"/>admin/todo/delete/";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["t"], "id", array()), "html", null, true);
            echo "\">Delete<a/>
             </td></tr>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['t'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "      </table>

";
    }

    public function getTemplateName()
    {
        return "admin_todos_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 19,  69 => 16,  65 => 15,  61 => 14,  57 => 13,  53 => 12,  48 => 11,  44 => 10,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Todo List{% endblock %}

{% block content %}

<a href=\"/admin/list/add\">Add Todo</a><br><br>
      <table border=\"1\">
          <tr><th>Name</th><th>Task</th><th>Due Date</th><th>Is Done</th></tr>
          {% for t in todoList %}
              <tr><td>{{t.name}}</td>
                  <td>{{t.task}}</td>
                  <td>{{t.dueDate}}</td>
                  <td>{{t.isDone}}</td>
                  <td><a href=\"/>admin/todo/edit/{{t.id}}\">Edit<a/>
                      <a href=\"/>admin/todo/delete/{{t.id}}\">Delete<a/>
             </td></tr>
          {% endfor %}
      </table>

{% endblock content %}
", "admin_todos_list.html.twig", "C:\\xampp\\htdocs\\ipd9\\slimtodo\\templates\\admin_todos_list.html.twig");
    }
}
