<?php

require_once 'vendor/autoload.php';

DB::$host = '127.0.0.1';
DB::$user = 'slimfirst1';
DB::$password = 'y47CQl3iSOjOhRPe';
DB::$dbName = 'slimfirst1';
DB::$port = 3333;
//y47CQl3iSOjOhRPe

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

$app->get('/hello/:name', function ($name) {
    echo "Hello, " . $name;
});

$app->get('/hello/:name/:age', function ($name, $age) use ($app) {
    //echo "Hello, $name, you are $age y/o";
    DB::insert('persons', array(
       'name' => $name,
       'age' => $age
    ));
    $app->render('hello.html.twig', array(
       'name' => $name,
       'age' => $age
    ));
});

$app->run();
        