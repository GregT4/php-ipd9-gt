<?php
session_start();

if (!isset($_SESSION['count'])){
    $_SESSION['count'] = 0;   
}

// BEGIN FORM FUNCTION
function get_form($t = "", $d = "") {
    // use of heredoc
    $form = <<< ENDOFIT
<form method="post">
    Task: <input type="text" name="task" value="$t"><br><br>
    DueDate: <input type="text" name="dueDate" value="$d"><br><br>
    <input type="submit" value="Add task"><br>    
</form>
ENDOFIT;
    return $form;
}

// Handle possible submission
if (isset($_POST['task'])) {
    // extract variables
    $task = $_POST['task'];
    $duedate = $_POST['dueDate'];
    
    // verify data submitted
    $errorList = array();
    
    if (strlen($task) < 2 || strlen($task) > 100 ) {
        array_push($errorList, "Task must be between 2 and 100 charecters");
    }
    if (!preg_match("/^[0-9]{4}-(0[1-9]|[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$duedate)) {
        array_push($errorList, "Due Date must be YYYY-MM-DD");
    }
    //
    if (!$errorList) {//Stae 2: successful submission
    $_  SESSION['count']++;
        // SQL INJECTION prevention ---- required on all queries
        $query = sprintf("INSERT INTO todos VALUES (NULL,'%s','%s')",
                     mysqli_real_escape_string($link, $task), 
                     mysqli_real_escape_string($link, $duedate)
        );
       $result = mysqli_query($link, $query);
        //echo "------------------" .$query;
        if (!$result) {
            echo "Error: Can not execute Query." . PHP_EOL;
            echo "Debugging errno: " . mysqli_errno($link) . PHP_EOL;
            echo "Debugging error: " . mysqli_errno($link) . PHP_EOL;
            exit;
        }
        echo '<p>Success! Message posted!</p>';
    }else {
        //State 3: failed submission
        echo get_form($task);

        echo '<p class="error">Error in your shout:</p>';
        echo "\n<ul>\n";
        foreach ($errorList as $error) {
            echo "<li>$error</li>";
        }
        echo "</ul>\n";
    }
    //echo get_form($name);
} else {
    // first show
    echo get_form();
}
