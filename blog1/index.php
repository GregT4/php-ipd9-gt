<?php

require_once 'db.php';

$query = " SELECT articles.id as id, users.username as username, "
        . " articles.title as title, articles.body as body "
        . " From articles, users "
        . " Where users.id = articles.authorId "
        . " Order BY id DESC ";
$result = mysqli_query($link, $query);
if (!$result){
    echo "Error executing SQL query: " . PHP_EOL;
    echo "Debugging errno: " . mysqli_errno($link) . PHP_EOL;
    echo "Debugging error: " . mysqli_error($link) . PHP_EOL;
    exit;
}
while ($article = mysqli_fetch_assoc($result)){
    printf("<p><a href=\"article_view.php$=%s\"%s by <i>%s<\i><\a><\p>",
            $article['id'], $article['title'], $article['username']);
}
         
                  