<h1>User Registration</h1>

<?php

require_once 'db.php';

function get_form($u = "", $e = "") {
    
    // example of "heredoc" use
    $form = <<< ENDOFIT
<form method="post">
    Username: <input types="text" name="username" value="$u"><br>
    Email: <input types="email" name="email" value="$e"><br>
    Password: <input types="password" name="pass1"><br>
    Password (repeated): <input types="password" name="pass2"><br>
    <input type="submit" value="Register">
</form>
ENDOFIT;
    return $form;
}

/* examples for debugging */
/*
  echo "<pre>\nPOST:\n";
  print_r($_POST);
  echo "</pre>\n";

  echo "<pre>\nSESSION:\n";
  print_r($_SESSION);
  echo "</pre>\n";
 */

if (isset($_POST['username'])){
    // extract the variables
    $username = $_POST['username'];
    $email = $_POST['email'];
    $pass1 = $_POST['pass1'];
    $pass2 = $_POST['pass2'];
    // verify data submitted
    $errorList = array();
    if (strlen($username) < 6 || strlen($username) > 50){
        array_push($errorList, "Username length must be between 6 and 50 characters");
    }
    if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE){
        array_push($errorList, "Email seems invalid");
    }
    if (strlen($pass1) < 6 || strlen($pass1) > 100){
        array_push($errorList, "Password length must be between 6 and 100 characters"); 
    }
    if ($pass1 != $pass2){
        array_push($errorList, "Passwords do not match");
    }
    //TODO: check username is only composed from lowercase letters and numbers
    //TODO: check password contains required variety of characters
    //TODO: check email is not already in use
    //TODO: check username is not already in use
      
    // if errorList is false(empty)
    if (!$errorList){
       // state 2: successful submission
        
       // escaping to ensure no sql injection is possible
        // MORE CONVOLUTED VERSION
        /*$query = "INSERT INTO users VALUES (NULL, "
                . "'" . mysqli_real_escape_string($link, $username)."', "
                . "'" . mysqli_real_escape_string($link, $email)."', "
                . "'" . mysqli_real_escape_string($link, $pass1)."')"; */
      
       // prefered version to ensure no sql injection
       $query = sprintf("INSERT INTO users VALUES (NULL, '%s', '%s', '%s')",
                  mysqli_real_escape_string($link, $username),
                  mysqli_real_escape_string($link, $email),
                  mysqli_real_escape_string($link, $pass1)
               );
             
       $result = mysqli_query($link, $query);
       if (!$result){
           echo "Error executing SQL query: " . PHP_EOL;
           echo "Debugging errno: " . mysqli_errno($link) . PHP_EOL;
           echo "Debugging error: " . mysqli_error($link) . PHP_EOL;
           exit;
       }
       echo '<p>Success! You may now <a href="login.php">Login</a></p>';
    } else {
       // state 3: failed submission
       echo get_form($username, $email);
       echo '<p class="error">Errors in your submission:</p>';
       echo "\n<ul>\n";
       foreach ($errorList as $error){
           echo "<li>$error</li>\n";
       }
        echo "\n<ul>\n";
    } 
} else {
    // state 1: first show
    echo get_form();
}
