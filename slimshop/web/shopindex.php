<?php

session_cache_limiter(false);
session_start();

require_once '../vendor/autoload.php';

//DB::$host = '127.0.0.1';
DB::$user = 'slimshop';
DB::$password = 'NcxIQ186TRBq7nX1';
DB::$dbName = 'slimshop';
DB::$port = 3333;
DB::$encoding = 'utf8';
//NcxIQ186TRBq7nX1
// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/../cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/../templates');

//pass info to all templates (similar globals)
if (!isset($_SESSION['user'])){
    $_SESSION['user'] = array();
}

$twig = $app->view()->getEnvironment();
$twig->addGlobal('user', $_SESSION['user']);

$app->get('/', function() use ($app) {
    $app->render('index.html.twig');
});

//State 1: First show
$app->get('/register', function() use ($app) {
    $app->render('register.html.twig');
});

//Receiving a submission
$app->post('/register', function() use ($app){
   // extract variables
   $email = $app->request()->post('email');
   $pass1 = $app->request()->post('pass1');
   $pass2 = $app->request()->post('pass2');
   //list of values to retain after a failed submission
   $valueList = array('email' => $email);
     // check for errors and collect error messages
     $errorList = array();
     if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE){
         array_push($errorList, "Email is invalid");
     } else {
         $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
         if ($user) {
             array_push($errorList, "Email already in use");
         }
     }
     if ($pass1 != $pass2) {
         array_push($errorList, "Passwords do not match");
     } else {
         if (strlen($pass1) < 6) {
             array_push($errorList, "Password too short, must be 6 characters or longer");
         }
         if (preg_match('/[A-Z]/', $pass1) != 1 
          || preg_match('/[a-z]/', $pass1) != 1 
          || preg_match('/[0-9]/', $pass1) != 1) {
             array_push($errorList, "Password must contain at least on lowercase, " 
                     . "one uppercase letter and a digit");
         }
     }
     //
     if ($errorList) {
         $app->render('register.html.twig', array(
             'errorList' => $errorList, 
             'v' => $valueList
         )); 
     } else {
         DB::insert('users', array(
             'email' => $email, 
             'password' => $pass1
       ));
       $app->render('registration_success.html.twig');
     }
});

//Ajax: is user with this email already registered?
$app->get('/ajax/emailused/:email', function($email){
   $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
   //echo json_encode($user, JSON_PRETTY_PRINT);
   echo json_encode($user != null);
});


// HOMEWORK 1: implement login form
$app->get('/login', function() use ($app) {
    $app->render('login.html.twig');
});

$app->post('/login', function() use ($app) {
    $email = $app->request()->post('email');
    $pass = $app->request()->post('pass');
    // verification    
    $error = false;
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    if (!$user) {
        $error = true;
    } else {
        if ($user['password'] != $pass) {
            $error = true;
        }
    }
    // decide what to render
    if ($error) {
        $app->render('login.html.twig', array("error" => true));
    } else {
        unset($user['password']);
        $_SESSION['user'] = $user;
        $app->render('login_success.html.twig');
    }               
});

// HOMEWORK 2: find and implement any tutorial about PHP file upload.
// create a new pure-PHP project to do it in

$app->get('/admin/product/add', function() use ($app) {
    $app->render('admin_product_add.html.twig');
});

$app->post('/admin/product/add', function() use ($app){
    $name = $app->request()->post('name');
    $description = $app->request()->post('description');
    $images = isset($_FILES['image']) ? $_FILES['image'] : array();
    //
    $errorList = array();
    if (strlen($name) < 2 || strlen($name) >100 ) {
          array_push($errorList, "Name must be 2-100 characters long.");
     }
     if (strlen($description) < 2 || strlen($description) >1000 ) {
          array_push($errorList, "Description must be 2-1000 characters long.");
     }
     if (!image) {
          array_push($errorList, "Image is required to create a product.");
     }else {
         $imageInfo - getimagesize($image['tmp_name']);
         if (!imageinfo) {
          array_push($errorList, "File does not look like a valid image.");
         }
     }
     //
     if ($errorList){
         $app->render("admin_product_add.html.twig", array(
             "errorList" => $errorList
         ));
     } else {
         // FIXME: opened a security hole here! ..
        $imagePath = "uploads/" . $image['name'];
        move_uploaded_file($image["tmp_name"], $imagePath);
        DB::insert('products', array(
            "name" => $name,
            "description" => $description,
            "imagePath" => $imagePath
        ));
        $app->render("admin_product_add_success.html.twig", array(
            "imagePath" => $imagePath
        ));
     }
});

// HOMEWORK: implement UPDATE/edit of an existing product
$app->get('/admin/product/edit/:id', function($id) use ($app) {
    $app->render("admin_product_add.html.twig");    
});

// NOTE: allow user NOT to replace image with a new one
// in other words if no image is uploaded you keep the existing one
$app->post('/admin/product/edit/:id', function($id) use ($app) {
});

// HOMEWORK: implement a table of existing products with links for editing
$app->get('/admin/product/list', function() use ($app) {
    $app->render("admin_product_list.html.twig");    
});


$app->run();
