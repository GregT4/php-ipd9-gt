<html>
  <head>
      <meta charset="UTF-8">
      <title></title>
  </head>
  <body>
      <a href="addtodo.php">Add Task</a><br><br>
      <table border="1">
          <tr><th>Task</th>
          <th>DueDate</th></tr>
      </table>
          <?php
      require_once 'db.php';
      
      $queryselect = "SELECT * FROM todos";
      
      $result = mysqli_query($link, $queryselect);
      
      if(!$result){
          die("Error executing query [$queryselect]: " . mysqli_error($link));
      }
      $datarows = mysqli_fetch_all($result, MYSQLI_ASSOC);
      
      foreach($datarows as $row){
      $task = $row['task'];
      $duedate = $row['dueDate'];
      
      echo "<tr><td>$task</td><td>$duedate</td></tr>";
      }
      
      echo "<p>You added the task in this browsing session " . $_SESSION['count'] . " times.</p>";
      
      ?>
</table>
  </body>
</html>
