<h1>Login</h1>

<?php

require_once 'db.php';

function get_form($login = "") {
    // example of "heredoc" use
    $form = <<< ENDOFIT
<form method="post">
    Username or Email: <input types="text" name="login" value="$login"><br>
    Password: <input types="password" name="pass"><br>
    <input type="submit" value="Login">
</form>
ENDOFIT;
    return $form;
}

// FIXME: SQL injection in register and login

if (isset($_POST['login'])) {
    // extract the variables
    $login = $_POST['login'];
    $pass = $_POST['pass'];
    
    //TODO: Ensure no SQL Injection
    $query = "SELECT * FROM users WHERE (username = '$login' OR email = '$login')";
    
    $result = mysqli_query($link, $query);
    if (!$result) {
        echo "Error executing SQL query: " . PHP_EOL;
        echo "Debugging errno: " . mysqli_errno($link) . PHP_EOL;
        echo "Debugging error: " . mysqli_error($link) . PHP_EOL;
        exit;
    }
    $row = mysqli_fetch_assoc($result);
    //
    $error = false;
    if (!$row) {
        $error = true;
    } else {
        if ($row['password'] != $pass) {
            $error = true;
        }
    }
    if ($error) {
        // state 3: failed submission
        echo get_form($login);
        echo '<p class="error">Login credentials invalid. Try again!</p>';
    } else {
        // STATE 2: successful submission
        // TODO: session variables
        unset($row['password']);
        $_SESSION['user'] = $row;
        echo "<p>Login successful!</p>";
    }
    } else {
    // STATE 1: first show
    echo get_form();
}
