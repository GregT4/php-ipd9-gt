<h1>Add Article</h1>

<?php

function get_form($t = "", $b ="") {
    // example of "heredoc" use
    $form = <<< ENDOFIT
<form method="post">
    Title: <input types="text" name="title" value="$t"><br>
    <textarea name="body">$b</textarea><br>
    <input type="submit" value="Add Article">
</form>
ENDOFIT;
    return $form;
}

require_once 'db.php';

if (!isset($_SESSION['user'])){
    echo "<h1>Access denied</h1>\n";
    echo "<p>You must be logged in to create an article.</p>";
    exit;
}

$userId = $_SESSION['user']['id'];

// HOMEWORK TODO: handle 3-state form submission to create an article
if (isset($_POST['title'])){
    $title = $_POST['title'];
    $body = $_POST['body'];
    //
    $errorList = array();
    if (strlen($title) < 5 || strlen($title) > 200){
        array_push($errorList, "Title length must be between 5 and 200 characters");        
    }
    if (strlen($body) < 5 || strlen($body) > 10000){
        array_push($errorList, "Body length must be between 5 and 10000 characters");       
    }
    if (!$errorList){
       // state 2: successful submission
       $query = sprintf("INSERT INTO articles VALUES (NULL, '%s', '%s', '%s')",
                  mysqli_real_escape_string($link, $userid),
                  mysqli_real_escape_string($link, $title),
                  mysqli_real_escape_string($link, $body)
               );
       $result = mysqli_query($link, $query);
       if (!$result){
           echo "Error executing SQL query: " . PHP_EOL;
           echo "Debugging errno: " . mysqli_errno($link) . PHP_EOL;
           echo "Debugging error: " . mysqli_error($link) . PHP_EOL;
           exit;
       }
       $id = mysqli_insert_id($link);
       echo '<p>Success! You may now <a href=\"article_view.php?id=$id\">'
              . "view your article</a>.</p>"; 
    } else {
       // state 3: failed submission
       echo get_form($title, $body);
       echo '<p class="error">Errors in your submission:</p>';
       echo "\n<ul>\n";
       foreach ($errorList as $error){
           echo "<li>$error</li>\n";
       }
        echo "\n<ul>\n";
    } 
} else {
    // STATE 1: first show
    echo get_form();
}



